package com.celites.sharechannels;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ceelites.devutils.ConstantMethods;
import com.ceelites.devutils.SharedPreferenceUtils;
import com.ceelites.librariesdemo.FakeDataProvider;
import com.ceelites.librariesdemo.LocalDataProvider;
import com.celites.sharechannels.adapters.ChannelsAdapter;
import com.celites.sharechannels.models.Channel;
import com.celites.sharechannels.models.Contacts;
import com.celites.sharechannels.models.PushbulletInfo;
import com.celites.sharechannels.models.PushbulletSubscriptions;
import com.celites.sharechannels.models.SelfUserModel;
import com.celites.sharechannels.utils.CircleTransform;
import com.celites.sharechannels.utils.LoadingRecyclerView;
import com.celites.sharechannels.utils.PushbulletApiService;
import com.celites.sharechannels.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import rx.Observer;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment
		extends Fragment {

	PushbulletInfo info;

	public MainActivityFragment() {
	}

	public static MainActivityFragment newInstance() {
		MainActivityFragment fragment = new MainActivityFragment();
		return fragment;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 2488 && resultCode == Activity.RESULT_OK) {
			String authToken = SharedPreferenceUtils.initWith(getActivity()).getString("AccessToken", "");
			getCurrentUserDetails(authToken);
		}
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.appCompatActivity = activity instanceof AppCompatActivity ? ((AppCompatActivity) activity) : null;
		utils = SharedPreferenceUtils.initWith(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.container = container;

		View view = inflater.inflate(R.layout.fragment_main, container, false);
		ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (appCompatActivity != null) {
			appCompatActivity.setSupportActionBar(currentUserCard);
		}

		channelsList.setHasFixedSize(true);

		mLayoutManager = new LinearLayoutManager(getActivity());
		mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		channelsList.setEmptyView(empty);
		channelsList.setLoadingView(progress);
		channelsList.setLayoutManager(mLayoutManager);


		preferenceUtils = SharedPreferenceUtils.initWith(getActivity());
		String authToken = preferenceUtils.getString("AccessToken", "");
		if (ConstantMethods.isEmptyString(authToken)) {
			authorize();
		} else {
			getCurrentUserDetails(authToken);
		}

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_main, menu);
		LocalDataProvider.inflateMenu(getActivity(), inflater, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (!LocalDataProvider.isDebugHandled(getActivity(), item)) {

			if (id == R.id.action_reauthorize) {
				authorize();
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	private void authorize() {
		Intent intent = new Intent(getActivity(), PushbulletAuthActivity.class);
		if (ConstantMethods.isOnline(getActivity())) {
			startActivityForResult(intent, 2488);
		} else {
			Snackbar.make(currentUserCard, "Can not authorize while offline", Snackbar.LENGTH_LONG).show();
		}
	}

	private void getCurrentUserDetails(String authToken) {
		Snackbar.make(currentUserCard, "Getting Current User Details", Snackbar.LENGTH_LONG).show();
		info = new PushbulletInfo();
		PushbulletApiService service = Utils.getWebservice(authToken);
		if (ConstantMethods.isOnline(getActivity())) {
			AppObservable.bindFragment(this, service.getCurrentUser()).observeOn(AndroidSchedulers.mainThread())
			             .subscribe(new Observer<SelfUserModel>() {
				             @Override
				             public void onCompleted() {

				             }

				             @Override
				             public void onError(Throwable e) {
					             String message = "Something is wrong with authorization, please authorize again";
					             Snackbar.make(container, message, Snackbar.LENGTH_LONG).show();
				             }

				             @Override
				             public void onNext(SelfUserModel model) {
					             final FakeDataProvider provider = new FakeDataProvider();
					             userName.setText(provider.getFakeData(getActivity(), model.getName(), 2));
					             Glide.with(MainActivityFragment.this).load(model.getImageUrl()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
					                  .transform(new CircleTransform(getActivity())).placeholder(R.mipmap.ic_launcher).crossFade()
					                  .into(new SimpleTarget<GlideDrawable>(50, 50) {
						                  @Override
						                  public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
							                  if (!provider.loadFakeData(userImage)) {
								                  userImage.setImageDrawable(resource);
							                  }
						                  }
					                  });
				             }
			             });

			channelsList.updateLoadingView();
			AppObservable.bindFragment(this, service.getSubscriptions()).observeOn(AndroidSchedulers.mainThread())
			             .subscribe(new Observer<PushbulletInfo>() {
				             @Override
				             public void onCompleted() {

				             }

				             @Override
				             public void onError(Throwable e) {

				             }

				             @Override
				             public void onNext(PushbulletInfo info) {
					             ArrayList<PushbulletSubscriptions> activeSubscriptions = new ArrayList<PushbulletSubscriptions>();
					             for (PushbulletSubscriptions subscriptions : info.getSubscriptions()) {
						             if (subscriptions.isActive()) {
							             Channel channel = subscriptions.getChannel();
							             if (channel != null) {

								             activeSubscriptions.add(subscriptions);
							             }
						             }
					             }
					             info.setSubscriptions(activeSubscriptions);
					             updateCounts(info);
					             channlesAdapter = new ChannelsAdapter(getActivity(), activeSubscriptions);
					             channelsList.setAdapter(channlesAdapter);
					             ArrayList<Contacts> contacts = MainActivityFragment.this.info.getContacts();
					             if (!ConstantMethods.isArrayListEmpty(contacts)) {
						             channlesAdapter.setContacts(contacts);
					             }

				             }
			             });


			AppObservable.bindFragment(this, service.getContacts()).observeOn(AndroidSchedulers.mainThread())
			             .subscribe(new Observer<PushbulletInfo>() {
				             @Override
				             public void onCompleted() {

				             }

				             @Override
				             public void onError(Throwable e) {

				             }

				             @Override
				             public void onNext(PushbulletInfo info) {
					             if (channlesAdapter != null) {
						             channlesAdapter.setContacts(info.getContacts());
					             }

					             updateCounts(info);
				             }
			             });
		} else {
			empty.setText("You have to be online to get details");
			channelsList.setEmptyView(empty);
			channelsList.updateEmptyView();
			Snackbar.make(currentUserCard, "You have to be online to get details", Snackbar.LENGTH_LONG).show();
		}

	}

	private void updateCounts(PushbulletInfo info) {
		if (this.info.getSubscriptions() == null || this.info.getSubscriptions().isEmpty()) {
			if (info.getSubscriptions() != null && !info.getSubscriptions().isEmpty()) {
				this.info.setSubscriptions(info.getSubscriptions());
			}
		}
		if (this.info.getContacts() == null || this.info.getContacts().isEmpty()) {
			if (info.getContacts() != null && !info.getContacts().isEmpty()) {
				this.info.setContacts(info.getContacts());
			}
		}
		List<PushbulletSubscriptions> subscriptions1 = this.info.getSubscriptions();
		int subscriptions = -1;
		int friends = -1;
		if (subscriptions1 != null) {
			subscriptions = subscriptions1.size();
		}
		List<Contacts> contacts = this.info.getContacts();
		if (contacts != null) {
			friends = contacts.size();
		}
		userDetails.setText(getString(R.string.friends_subscriptions, subscriptions, friends));

	}

	private SharedPreferenceUtils utils;

	@Bind(R.id.channelsList)
	LoadingRecyclerView channelsList;
	private LinearLayoutManager mLayoutManager;
	private ChannelsAdapter channlesAdapter;
	private SharedPreferenceUtils preferenceUtils;
	@Bind(R.id.currentUserCard)
	Toolbar currentUserCard;
	private AppCompatActivity appCompatActivity;
	private ViewGroup container;
	@Bind(R.id.progress)
	ProgressBar progress;
	@Bind(android.R.id.empty)
	TextView empty;
	@Bind(R.id.userName)
	TextView userName;
	@Bind(R.id.userImage)
	ImageView userImage;
	@Bind(R.id.userDetails)
	TextView userDetails;
}
