package com.celites.sharechannels.models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by Prasham on 4/25/2015.
 */
public class ChannelInfo {

	private String iden;

	private String name;

	private String tag;

	private String description;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("website_url")
	private String websiteUrl;

	@SerializedName("subscriber_count")
	private int subscriberCount;

	@SerializedName("recent_pushes")
	private ArrayList<RecentPush> recentPushes = new ArrayList<RecentPush>();

	/**
	 * @return The iden
	 */
	public String getIden() {
		return iden;
	}

	/**
	 * @param iden
	 * 		The iden
	 */
	public void setIden(String iden) {
		this.iden = iden;
	}

	/**
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * 		The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return The tag
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * @param tag
	 * 		The tag
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * @return The description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 * 		The description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return The imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 * 		The image_url
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return The websiteUrl
	 */
	public String getWebsiteUrl() {
		return websiteUrl;
	}

	/**
	 * @param websiteUrl
	 * 		The website_url
	 */
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	/**
	 * @return The subscriberCount
	 */
	public int getSubscriberCount() {
		return subscriberCount;
	}

	/**
	 * @param subscriberCount
	 * 		The subscriber_count
	 */
	public void setSubscriberCount(int subscriberCount) {
		this.subscriberCount = subscriberCount;
	}

	/**
	 * @return The recentPushes
	 */
	public ArrayList<RecentPush> getRecentPushes() {
		return recentPushes;
	}

	/**
	 * @param recentPushes
	 * 		The recent_pushes
	 */
	public void setRecentPushes(ArrayList<RecentPush> recentPushes) {
		this.recentPushes = recentPushes;
	}

}
