package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class Channel {
	private String image_url;

	private String description;

	private String tag;

	private String name;

	private String iden;

	public Channel(String image_url, String description, String tag, String name, String iden) {
		this.image_url = image_url;
		this.description = description;
		this.tag = tag;
		this.name = name;
		this.iden = iden;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIden() {
		return iden;
	}

	public void setIden(String iden) {
		this.iden = iden;
	}

	@Override
	public String toString() {
		return "ClassPojo [image_url = " + image_url + ", description = " + description + ", tag = " + tag + ", name = " + name + ", iden = " + iden
		       + "]";
	}
}
