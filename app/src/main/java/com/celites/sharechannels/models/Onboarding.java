package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class Onboarding {


	private boolean app;

	private boolean extension;

	private boolean friends;

	/**
	 * @return The app
	 */
	public boolean isApp() {
		return app;
	}

	/**
	 * @param app
	 * 		The app
	 */
	public void setApp(boolean app) {
		this.app = app;
	}

	/**
	 * @return The extension
	 */
	public boolean isExtension() {
		return extension;
	}

	/**
	 * @param extension
	 * 		The extension
	 */
	public void setExtension(boolean extension) {
		this.extension = extension;
	}

	/**
	 * @return The friends
	 */
	public boolean isFriends() {
		return friends;
	}

	/**
	 * @param friends
	 * 		The friends
	 */
	public void setFriends(boolean friends) {
		this.friends = friends;
	}

}

