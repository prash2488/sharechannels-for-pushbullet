package com.celites.sharechannels.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prasham on 4/25/2015.
 */
public class PushbulletInfo {

	private List<Object> accounts = new ArrayList<Object>();

	private List<Object> aliases = new ArrayList<Object>();

	private List<Object> channels = new ArrayList<Object>();

	private List<Object> chats = new ArrayList<Object>();

	private List<Object> clients = new ArrayList<Object>();

	private ArrayList<Contacts> contacts = new ArrayList<Contacts>();

	private List<Object> devices = new ArrayList<Object>();

	private List<Object> grants = new ArrayList<Object>();

	private List<Object> pushes = new ArrayList<Object>();

	private ArrayList<PushbulletSubscriptions> subscriptions = new ArrayList<PushbulletSubscriptions>();

	/**
	 * @return The accounts
	 */
	public List<Object> getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts
	 * 		The accounts
	 */
	public void setAccounts(List<Object> accounts) {
		this.accounts = accounts;
	}

	/**
	 * @return The aliases
	 */
	public List<Object> getAliases() {
		return aliases;
	}

	/**
	 * @param aliases
	 * 		The aliases
	 */
	public void setAliases(List<Object> aliases) {
		this.aliases = aliases;
	}

	/**
	 * @return The channels
	 */
	public List<Object> getChannels() {
		return channels;
	}

	/**
	 * @param channels
	 * 		The channels
	 */
	public void setChannels(List<Object> channels) {
		this.channels = channels;
	}

	/**
	 * @return The chats
	 */
	public List<Object> getChats() {
		return chats;
	}

	/**
	 * @param chats
	 * 		The chats
	 */
	public void setChats(List<Object> chats) {
		this.chats = chats;
	}

	/**
	 * @return The clients
	 */
	public List<Object> getClients() {
		return clients;
	}

	/**
	 * @param clients
	 * 		The clients
	 */
	public void setClients(List<Object> clients) {
		this.clients = clients;
	}

	/**
	 * @return The contacts
	 */
	public ArrayList<Contacts> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts
	 * 		The contacts
	 */
	public void setContacts(ArrayList<Contacts> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return The devices
	 */
	public List<Object> getDevices() {
		return devices;
	}

	/**
	 * @param devices
	 * 		The devices
	 */
	public void setDevices(List<Object> devices) {
		this.devices = devices;
	}

	/**
	 * @return The grants
	 */
	public List<Object> getGrants() {
		return grants;
	}

	/**
	 * @param grants
	 * 		The grants
	 */
	public void setGrants(List<Object> grants) {
		this.grants = grants;
	}

	/**
	 * @return The pushes
	 */
	public List<Object> getPushes() {
		return pushes;
	}

	/**
	 * @param pushes
	 * 		The pushes
	 */
	public void setPushes(List<Object> pushes) {
		this.pushes = pushes;
	}

	/**
	 * @return The subscriptions
	 */
	public ArrayList<PushbulletSubscriptions> getSubscriptions() {
		return subscriptions;
	}

	/**
	 * @param subscriptions
	 * 		The subscriptions
	 */
	public void setSubscriptions(ArrayList<PushbulletSubscriptions> subscriptions) {
		this.subscriptions = subscriptions;
	}
}
