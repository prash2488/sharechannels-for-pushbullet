package com.celites.sharechannels;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.ceelites.devutils.SharedPreferenceUtils;


public class PushbulletAuthActivity
		extends AppCompatActivity {

	@Bind(R.id.authWebView)
	WebView authWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pushbullet_auth);
		String pushBulletAutUrl =
				"https://www.pushbullet.com/authorize?client_id=" + BuildConfig.CLIENT_ID + "&redirect_uri=" + BuildConfig.REDIRECTION_URL
				+ "&response_type=token&scope=everything";
		CookieManager.getInstance().setAcceptCookie(true);
		ButterKnife.bind(this);
		WebSettings settings = authWebView.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setDomStorageEnabled(true);
		authWebView.setWebViewClient(webviewClient);
		authWebView.loadUrl(pushBulletAutUrl);
	}

	@Override
	protected void onStop() {
		ButterKnife.unbind(this);
		super.onStop();
	}

	private WebViewClient webviewClient = new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith(BuildConfig.REDIRECTION_URL)) {
				int startIndex = BuildConfig.REDIRECTION_URL.length() + "#access_token".length() + 1;
				String accessToken = url.substring(startIndex);
				SharedPreferenceUtils.initWith(PushbulletAuthActivity.this).putString("AccessToken", accessToken);
				setResult(RESULT_OK);
				finish();
				return true;
			} else {
				return false;
			}
		}
	};

}
