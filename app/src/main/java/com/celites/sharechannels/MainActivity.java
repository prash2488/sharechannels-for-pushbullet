package com.celites.sharechannels;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


public class MainActivity
		extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics crashlytics = new Crashlytics();
		crashlytics.core.setString("CodeRevision", BuildConfig.GIT_SHA);
		Fabric.with(this, crashlytics);

		setContentView(R.layout.activity_main);
		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, MainActivityFragment.newInstance(), "tag").commit();
	}

}
