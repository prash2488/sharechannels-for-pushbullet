package com.celites.sharechannels;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ceelites.devutils.ConstantMethods;
import com.ceelites.devutils.SharedPreferenceUtils;
import com.ceelites.librariesdemo.FakeDataProvider;
import com.celites.sharechannels.adapters.ChannelsItemAdapter;
import com.celites.sharechannels.databinding.FragmentChannelsDetailBinding;
import com.celites.sharechannels.models.Channel;
import com.celites.sharechannels.models.ChannelInfo;
import com.celites.sharechannels.models.Contacts;
import com.celites.sharechannels.models.PushbulletInfo;
import com.celites.sharechannels.models.PushbulletSubscriptions;
import com.celites.sharechannels.models.SubscribeRequest;
import com.celites.sharechannels.utils.CircleTransform;
import com.celites.sharechannels.utils.LoadingRecyclerView;
import com.celites.sharechannels.utils.PushRequest;
import com.celites.sharechannels.utils.PushbulletApiService;
import com.celites.sharechannels.utils.Utils;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;


/**
 * A placeholder fragment containing a simple view.
 */
public class ChannelsDetailActivityFragment
		extends Fragment {

	public ChannelsDetailActivityFragment() {
	}

	private String tag;
	private PushbulletApiService service;
	private LinearLayoutManager mLayoutManager;
	private ChannelsItemAdapter adapter;
	private SharedPreferenceUtils preferenceUtils;
	private MenuItem item;
	private String iden;
	Toolbar channelDetailToolbar;
	private AppCompatActivity appCompatActivity;
	private ChannelInfo info;

	/**
	 * Instantiates new fragment.
	 *
	 * @param bundle
	 * 		: Any extra data to be passed to fragment, pass null if you don't want to pass anything.
	 *
	 * @return : Instance of current fragment.
	 */
	public static ChannelsDetailActivityFragment newInstance(@NonNull Bundle bundle) {
		ChannelsDetailActivityFragment channelsDetailFragment = new ChannelsDetailActivityFragment();
		channelsDetailFragment.setArguments(bundle);
		return channelsDetailFragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.appCompatActivity = activity instanceof AppCompatActivity ? ((AppCompatActivity) activity) : null;
		service = Utils.getWebservice(activity);
		preferenceUtils = SharedPreferenceUtils.initWith(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getData();
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_channels_detail, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		channelDetailToolbar = binding.channelDetailToolbar;
		recentPosts = binding.recentPosts;
		channelDescription = binding.channelDescription;
		emptyView = binding.emptyView;
		progress = binding.progress;
		if (appCompatActivity != null) {
			appCompatActivity.setSupportActionBar(channelDetailToolbar);
		}


		fakeDataProvider = new FakeDataProvider();

		recentPosts.setHasFixedSize(true);
		mLayoutManager = new LinearLayoutManager(getActivity());
		mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		recentPosts.setEmptyView(emptyView);
		recentPosts.setLoadingView(progress);
		recentPosts.setLayoutManager(mLayoutManager);

		if (ConstantMethods.isOnline(getActivity())) {
			AppObservable.bindFragment(this, service.getContacts()).observeOn(AndroidSchedulers.mainThread())
			             .subscribe(new Observer<PushbulletInfo>() {
				             @Override
				             public void onCompleted() {

				             }

				             @Override
				             public void onError(Throwable e) {

				             }

				             @Override
				             public void onNext(PushbulletInfo info) {
					             contacts = info.getContacts();
					             getActivity().supportInvalidateOptionsMenu();
				             }
			             });

			if (!ConstantMethods.isEmptyString(tag)) {
				recentPosts.updateLoadingView();
				AppObservable.bindFragment(this, service.getChannelInfo(tag)).observeOn(AndroidSchedulers.mainThread())
				             .subscribe(new Subscriber<ChannelInfo>() {


					             @Override
					             public void onCompleted() {

					             }

					             @Override
					             public void onError(Throwable e) {

					             }

					             @Override
					             public void onNext(ChannelInfo info) {
						             ChannelsDetailActivityFragment.this.info = info;


						             binding.setChannelInfo(info);
						             Glide.with(ChannelsDetailActivityFragment.this).load(info.getImageUrl())
						                  .diskCacheStrategy(DiskCacheStrategy.RESULT).placeholder(R.mipmap.ic_launcher).crossFade()
						                  .transform(new CircleTransform(getActivity())).into(new SimpleTarget<GlideDrawable>(50, 50) {
							             @Override
							             public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
								             if (!fakeDataProvider.loadFakeData(binding.channelLogo)) {
									             binding.channelLogo.setImageDrawable(resource);
								             }
							             }
						             });
						             adapter = new ChannelsItemAdapter(getActivity(), info.getRecentPushes());
						             recentPosts.setAdapter(adapter);
					             }
				             });
			}
		} else {
			emptyView.setText("You have to be online to get details");
			recentPosts.setEmptyView(emptyView);
			recentPosts.updateEmptyView();
			showSnackbarMessage("You have to be online to get details");
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_channels_detail, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		changeOptionsMenu(menu);
		super.onPrepareOptionsMenu(menu);
	}

	private void changeOptionsMenu(Menu menu) {
		item = menu.findItem(R.id.action_subscribe_unsubscribe);
		AppObservable.bindFragment(this, service.getSubscriptions()).observeOn(AndroidSchedulers.mainThread())
		             .subscribe(new Observer<PushbulletInfo>() {
			             @Override
			             public void onCompleted() {

			             }

			             @Override
			             public void onError(Throwable e) {

			             }

			             @Override
			             public void onNext(PushbulletInfo info) {
				             ArrayList<PushbulletSubscriptions> activeSubscriptions = new ArrayList<PushbulletSubscriptions>();
				             for (PushbulletSubscriptions subscriptions : info.getSubscriptions()) {
					             if (subscriptions.isActive()) {
						             Channel channel = subscriptions.getChannel();
						             if (channel != null) {
							             if (channel.getTag().equalsIgnoreCase(tag)) {
								             iden = subscriptions.getIden();
								             item.setTitle("Unsubscribe");
								             break;
							             }
						             }
					             }
				             }


			             }
		             });
		MenuItem shareItem = menu.findItem(R.id.action_share);
		boolean empty = ConstantMethods.isArrayListEmpty(contacts);
		shareItem.setVisible(!empty);
		SubMenu menu1 = shareItem.getSubMenu();
		if (!empty) {
			menu1.clear();
			int i = 0;
			for (Contacts contact : contacts) {
				menu1.add(1, 2488 + i, i, fakeDataProvider.getFakeData(getActivity(), contact.getName(), 2));
				i++;
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return handleMenuItemClick(item);
	}

	private boolean handleMenuItemClick(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_subscribe_unsubscribe) {

			if (item.getTitle().toString().equalsIgnoreCase("Unsubscribe")) {
				final Snackbar make = Snackbar.make(channelDetailToolbar, "Unsubscribing from channel", Snackbar.LENGTH_LONG);

				final Observable<Long> observable = AppObservable.bindFragment(this, Observable.timer(2750, TimeUnit.MILLISECONDS));

				final Subscription subscribe = observable.subscribe(new Observer<Long>() {
					@Override
					public void onCompleted() {
						unsubscribe();
					}

					@Override
					public void onError(Throwable e) {

					}

					@Override
					public void onNext(Long aLong) {

					}
				});
				make.setAction("UNDO", new OnClickListener() {
					@Override
					public void onClick(View v) {

						subscribe.unsubscribe();

					}
				});
				make.show();


				//				unsubscribe();
			} else {
				SubscribeRequest request = new SubscribeRequest();
				request.setChannel_tag(tag);
				AppObservable.bindFragment(this, service.subscribeToAChannel(request)).observeOn(AndroidSchedulers.mainThread())
				             .subscribe(new Subscriber<PushbulletSubscriptions>() {
					             @Override
					             public void onCompleted() {
						             String subscribed = "Subscribed";
						             showSnackbarMessage(subscribed);
						             getActivity().supportInvalidateOptionsMenu();
					             }

					             @Override
					             public void onError(Throwable e) {
						             String text = "Subscription failed " + e.getMessage();
						             showSnackbarMessage(text);
					             }

					             @Override
					             public void onNext(PushbulletSubscriptions subscriptions) {

					             }
				             });

			}
		} else if (id >= 2488 && item.getGroupId() == 1) {
			int index = item.getOrder();

			Contacts contact = contacts.get(index);
			PushRequest request = PushRequest.creteFromDefault();
			request.setTitle(info.getName());
			request.setEmail(contact.getEmail());
			request.setUrl("http://prashamtrivedi.github.io/ShareChannels/?tag=" + tag);
			service.sendInvite(request).subscribe(new Subscriber<Void>() {
				@Override
				public void onCompleted() {
					String text = "Subscription sent successfully";
					Snackbar.make(channelDetailToolbar, text, Snackbar.LENGTH_LONG).show();
				}

				@Override
				public void onError(Throwable e) {
					Log.d("Error", e.getMessage());
				}

				@Override
				public void onNext(Void aVoid) {

				}
			});
			return true;
		}
		return true;
	}

	private void unsubscribe() {
		AppObservable.bindFragment(this, service.deleteSubscription(iden)).observeOn(AndroidSchedulers.mainThread())
		             .subscribe(new Subscriber<Void>() {
			             @Override
			             public void onCompleted() {
				             String text = "Subscription Deleted";
				             getActivity().supportInvalidateOptionsMenu();
				             showSnackbarMessage(text);
			             }

			             @Override
			             public void onError(Throwable e) {
				             String text = "Subscription can not be deleted " + e.getMessage();
				             showSnackbarMessage(text);
			             }

			             @Override
			             public void onNext(Void aVoid) {

			             }
		             });
	}

	private void showSnackbarMessage(String message) {
		Snackbar.make(channelDetailToolbar, message, Snackbar.LENGTH_LONG).show();
	}

	private void getData() {
		Bundle arguments = getArguments();
		tag = arguments.getString("channel_tag");
	}

	LoadingRecyclerView recentPosts;
	TextView channelDescription;
	private FragmentChannelsDetailBinding binding;
	private ArrayList<Contacts> contacts;
	private TextView emptyView;
	private ProgressBar progress;
	private FakeDataProvider fakeDataProvider;
}
