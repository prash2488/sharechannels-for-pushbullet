package com.celites.sharechannels.utils;

import com.celites.sharechannels.models.ChannelInfo;
import com.celites.sharechannels.models.PushbulletInfo;
import com.celites.sharechannels.models.PushbulletSubscriptions;
import com.celites.sharechannels.models.SelfUserModel;
import com.celites.sharechannels.models.SubscribeRequest;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Prasham on 4/25/2015.
 */
public interface PushbulletApiService {
	String PUSHBULLET_API_ENDPOINT = "https://api.pushbullet.com/v2";

	@GET("/users/me")
	Observable<SelfUserModel> getCurrentUser();

	@GET("/contacts")
	Observable<PushbulletInfo> getContacts();

	@GET("/subscriptions")
	Observable<PushbulletInfo> getSubscriptions();

	@POST("/subscriptions")
	Observable<PushbulletSubscriptions> subscribeToAChannel(@Body SubscribeRequest request);

	@GET("/channel-info")
	Observable<ChannelInfo> getChannelInfo(@Query("tag") String channelTag);

	@DELETE("/subscriptions/{iden}")
	Observable<Void> deleteSubscription(@Path("iden") String outerIden);

	@POST("/pushes")
	Observable<Void> sendInvite(@Body PushRequest request);
}
